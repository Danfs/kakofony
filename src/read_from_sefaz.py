from collections import defaultdict

from bs4 import BeautifulSoup, Tag
import requests

REPORT: dict[tuple[str, str], list[dict[str, str]]] = defaultdict(list)

def read_nfce_from_sefaz(link: str):
    page_content = requests.get(link)

    soup = BeautifulSoup(page_content.text, "html.parser")

    table = soup.find("table", {"id": "tabResult"})
    assert isinstance(table, Tag)

    items = table.find_all("tr", recursive=False)
    for item in items:
        assert isinstance(item, Tag)
        nome = _read_nome_from_table_row(item)
        codigo = _read_codigo_from_table_row(item)
        qtdd = _read_qtdd_from_table_row(item)
        valor_unit = _read_valor_unit_from_table_row(item)
        valor_total = _read_valor_total_from_table_row(item)
        print(f"{nome=}")
        print(f"{codigo=}")
        print(f"{qtdd=}")
        print(f"{valor_unit=}")
        print(f"{valor_total=}")
        print("-----")
        info = {
            "qtdd": qtdd,
            "valor_unit": valor_unit,
            "valor_total": valor_total,
        }
        REPORT[(codigo, nome)].append(info)

def _read_nome_from_table_row(item: Tag) -> str:
    nome_html = item.find("span", {"class": "txtTit"})
    assert nome_html is not None

    return nome_html.text.strip()

def _read_codigo_from_table_row(item: Tag) -> str:
    codigo_html = item.find("span", {"class": "RCod"})
    assert codigo_html is not None

    codigo_text = codigo_html.text.strip()

    return codigo_text.split(" ")[1][:-1]

def _read_qtdd_from_table_row(item: Tag) -> str:
    qtdd_html = item.find("span", {"class": "Rqtd"})
    assert qtdd_html is not None

    qtdd_text = qtdd_html.text.strip()

    return qtdd_text.split(":")[-1]

def _read_valor_unit_from_table_row(item: Tag) -> str:
    valor_html = item.find("span", {"class": "RvlUnit"})
    assert valor_html is not None

    valor_text = valor_html.text.strip()

    return valor_text.split("\xa0")[-1]

def _read_valor_total_from_table_row(item: Tag) -> str:
    valor_html = item.find("td", {"class": "txtTit noWrap"})
    assert valor_html is not None

    valor_text = valor_html.text.strip()

    return valor_text.split("Total")[-1]

if __name__ == "__main__":
    for link in (
    ):
        read_nfce_from_sefaz(link)
    from pprint import pprint
    pprint(REPORT)
