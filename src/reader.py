from data_structures import PurchaseReport, is_month, ItemReport, Month
from menu_strings import DATE_PROMPT, ITEM_NAME_PROMPT, ITEM_QUANTITY_PROMPT, ITEM_TAGS_PROMPT, ITEM_VALUE_PROMPT, TOTAL_VALUE_PROMPT

UPPERCASE_NEGATIVE_STRINGS = frozenset(("N", "NO", "NÃO", "FALSE"))

def get_month() -> tuple[Month, str]:
    while True:
        match input(DATE_PROMPT).split():
            case [month, year]:
                upper_month = month.upper()
                if not is_month(upper_month):
                    print(f"Mês ({month}) não reconhecido")
                elif not(year.isdigit() and len(year) == 4):
                    print(f"Ano ({year}) em formato não reconhecido")
                else:
                    return upper_month, year
            case _: 
                print("Erro! Passe mês e ano separados por espaço, e nada a mais ou a menos")

def _get_total_value() -> int:
    while True:
        match input(TOTAL_VALUE_PROMPT).split():
            case [value] if value.isdigit():
                return int(value)
            case [value]:
                print(f"O valor ({value}) não foi reconhecido pelo sistema")
            case _:
                print("Insira apenas o valor da compra, em centavos, sem espaços")

def get_item() -> ItemReport:
    item_name = input(ITEM_NAME_PROMPT)

    qtty  = _input_single_integer(ITEM_QUANTITY_PROMPT)
    value = _input_single_integer(ITEM_VALUE_PROMPT)

    tags = {x.upper() for x in input(ITEM_TAGS_PROMPT).split()}

    return {
        'name': item_name,
        'quantity': qtty,
        'total_price': value,
        'tags': tags
    }

def get_purchase() -> PurchaseReport:
    total_value = _get_total_value()

    purchase_report: PurchaseReport = {
        'total_value': total_value,
        'items': []
    }
    while input("\nDeseja adicionar um item? (Y/n)").upper() not in UPPERCASE_NEGATIVE_STRINGS:
        item = get_item()
        purchase_report["items"].append(item)

    return purchase_report

def _input_single_integer(prompt_text: str) -> int:
    while True:
        match input(prompt_text).split():
            case [x] if x.isdigit():
                return int(x)
            case [_]:
                print("Insira apenas dígitos!")
            case _:
                print("Insira apenas um número inteiro. Não insira unidades, letras, nem separadores decimais como vírgulas ou pontos")
