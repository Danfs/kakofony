from contextlib import AbstractContextManager
from types import TracebackType
from typing import Protocol, Self

from data_structures import Month, PurchaseReport, SavedStructure

class AbstractLoader(AbstractContextManager['AbstractLoader'], Protocol):
    def __enter__(self) -> Self:
        '''
        Should run every setup necessary
        and return the Context Manager
        '''
        ...

    def __exit__(self, exc_type: type[BaseException] | None, exc_value: BaseException | None, traceback: TracebackType | None) -> bool | None:
        '''
        Should run every persistency necessary, and then
        surpress/treat the exception, if any and reasonable,
        or return False/None to propagate the exception
        '''
        ...

    def save_purchase_report(self, report: PurchaseReport, date: tuple[Month, str]) -> None:
        '''
        Should save the `report`, registering it
        as a purchase made on the `date` passed
        '''
        ...

    def monthly_report(self, date: tuple[Month, str]) -> None:
        '''I still don't really know'''
        ...

    def import_data(self, data: SavedStructure) -> None:
        '''
        Should read the information in `data` and
        save it on its own persistency mechanism,
        ignoring whatever data was previously saved
        '''
        ...

    def export_data(self) -> SavedStructure:
        '''
        Should compile all of its data into a `SavedStructure`,
        so that it can be imported by any other AbstractLoader
        '''
        ...
