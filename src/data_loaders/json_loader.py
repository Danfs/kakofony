import datetime
import json
from types import TracebackType
from typing import Any, Final, Self

from .abstract_loader import AbstractLoader

from data_structures import Month, PurchaseReport, SavedStructure

class JsonLoader(AbstractLoader):
    '''
    The JSON loader is the most simple Loader available.
    It reads the data from a local .json file on startup,
    loads all the data into memory during runtime,
    and saves the data back to the file when it stops running
    '''
    data: SavedStructure
    SAVEFILE: Final = "expenses.json"
    ENCODING: Final = "UTF-8"

    def __enter__(self) -> Self:
        try:
            with open(self.SAVEFILE, "r", encoding=self.ENCODING) as f:
                self.data = json.load(f, cls=_CustomDecoder)
        except FileNotFoundError:
            print(f"Não encontrado um arquivo {self.SAVEFILE}. Criando um estado inicial zerado")
            self.data = {
                "reports": {},
                "all_tags": set()
            }
        except json.JSONDecodeError as e:
            print(f"Falha ao ler dados do arquivo {self.SAVEFILE}")
            raise e
        return self

    def __exit__(self, exc_type: type[BaseException] | None, exc_value: BaseException | None, traceback: TracebackType | None) -> bool | None:
        with open(self.SAVEFILE, "w", encoding=self.ENCODING) as f:
            json.dump(self.data, f, cls=_CustomEncoder)
        return None

    def save_purchase_report(self, report: PurchaseReport, date: tuple[Month, str]) -> None:
        month, year = date
        annual_report = self.data['reports'].setdefault(year, {})
        monthly_report = annual_report.setdefault(month, {})

        timestamp = datetime.datetime.now().strftime("%d_%H:%M:%S")
        monthly_report[timestamp] = report
        self.data["all_tags"].update(*(x["tags"] for x in report["items"]))

    # TODO
    def monthly_report(self, date: tuple[Month, str]) -> None:
        ...

    def export_data(self) -> SavedStructure:
        return self.data

    def import_data(self, data: SavedStructure) -> None:
        self.data = data

class _CustomDecoder(json.JSONDecoder):
    def __init__(self) -> None:
        super().__init__(object_hook=self._read_object_hook)

    @staticmethod
    def _read_object_hook(entry: dict[str, Any]) -> dict[str, Any]:
        if tags := entry.get('tags'):
            entry['tags'] = set(tags)
        if all_tags := entry.get('all_tags'):
            entry['all_tags'] = set(all_tags)
        return entry

class _CustomEncoder(json.JSONEncoder):
    def default(self, o: Any) -> Any:
        match o:
            case set():
                return list(o) # type: ignore
            case _:
                return super().default(o)

if __name__ == "__main__":
    loader: AbstractLoader = JsonLoader()
