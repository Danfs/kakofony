import sqlite3
from types import TracebackType
from typing import Self

from data_structures import Month, PurchaseReport

from .abstract_loader import AbstractLoader

class SQLiteLoader(AbstractLoader):
    connection: sqlite3.Connection

    def __enter__(self) -> Self:
        self.connection = sqlite3.connect("expenses.sqlite3")
        return self

    def __exit__(self, exc_type: type[BaseException] | None, exc_value: BaseException | None, traceback: TracebackType | None) -> bool | None:
        self.connection.commit()
        self.connection.close()

    def save_purchase_report(self, report: PurchaseReport, date: tuple[Month, str]) -> None:
        cursor = self.connection.cursor()
        
        cursor.close()


a: AbstractLoader = SQLiteLoader()