from menu_strings import COMMAND_PROMPT
import reader
import data_loaders

def get_loader() -> data_loaders.AbstractLoader:
    return data_loaders.JsonLoader()

def main():
    with get_loader() as loader:
        while True:
            month, year = reader.get_month()

            while True:
                match input(COMMAND_PROMPT):
                    case '1':
                        print("Ainda não tem resumo LOL")
                    case '2':
                        new_purchase = reader.get_purchase()
                        loader.save_purchase_report(new_purchase, (month, year))
                    case '3':
                        break
                    case _:
                        print("Comando não reconhecido!")
            break

if __name__ == "__main__":
    main()
