from typing import Literal, TypedDict, TypeGuard

Month = Literal['JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'JUN', 'JUL', 'AGO', 'SET', 'OUT', 'NOV', 'DEZ']
_MONTHS: frozenset[Month] = frozenset(('JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'JUN', 'JUL', 'AGO', 'SET', 'OUT', 'NOV', 'DEZ'))
def is_month(x: str) -> TypeGuard[Month]:
    return x in _MONTHS

class ItemReport(TypedDict):
    name: str
    quantity: int
    total_price: int
    tags: set[str]

class PurchaseReport(TypedDict):
    total_value: int
    items: list[ItemReport]

MonthlyReport = dict[str, PurchaseReport]

AnnualReport = dict[Month, MonthlyReport]

class SavedStructure(TypedDict):
    reports: dict[str, AnnualReport]
    all_tags: set[str]
