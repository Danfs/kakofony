DATE_PROMPT = """
Escolha um mês (sigla de 3 letras) e ano (4 dígitos), separados por espaço
Ex.: FEV 2024
> """

COMMAND_PROMPT = """
Você deseja ver o resumo desse mês, adicionar uma nova compra a ele, ou fechar o programa?
(1) Ver resumo
(2) Adicionar compra
(3) Sair
> """

TOTAL_VALUE_PROMPT = """
Insira o valor total da compra, em centavos (Ex.: 4 reais -> '400')
> """

ITEM_NAME_PROMPT = """
Insira o nome do item que você quer adicionar
> """

ITEM_QUANTITY_PROMPT = """
Insira a quantidade do item que foi comprada, em unidades ou gramas
> """

ITEM_VALUE_PROMPT = """
Insira o valor total pago no item, em centavos
> """

ITEM_TAGS_PROMPT = """
Insira as tags para esse item, separando-as com espaço
Tags são strings divididas via dot syntax e são case-insensitive
> """
